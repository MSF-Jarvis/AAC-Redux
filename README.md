# [__AndroidAutoCompiler-Redux__](http://msfjarvis.tk/AAC-Redux)

This repository uses nearly the same codebase and ideas as the original [AAC2](https://github.com/AndroGeek974/AndroidAutoCompiler2), but with a brand-new popup-based interface to take the pain out of managing frames. This version of AAC doesn't offer something more than AAC2, rather less, yet is more easier to use than it's big daddy.

# HOW TO USE

The script is relatively easy to use since it has a GUI, but here's what'll still need to be done.

1. Place the AAC-Redux.py file in the home directory of your UBUNTU PC (Yes, we only support Ubuntu for now.)
2. Open a terminal (Ctrl + Alt + T) and type `python3 AAC-Redux.py`
 

# Dependencies

The script only requires python3 be installed on your system, using this 

    sudo apt-get install python3

# Other Info

I and other contributors in no way lay claim to the original idea and codebase. I will continue working on this, let's say fork, of AAC2 and also contribute back to the original project whenever my help is askied for or required.
