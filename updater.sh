#Updater shell for AAC-Redux
#
#The script checks whether the repository was git cloned or downloaded as a zips
#
#In case of git clone, the shell executes git pull and syncs it up
#If zip was downloaded, then wget is used to fetch the zip, which is then unzipped on the current directory,
#wiping off existing files.

curl https://raw.githubusercontent.com/MSF-Jarvis/AAC-Redux/master/version.md >> updatecheck

a = cat version.md
b = cat updatecheck
if [ $b > $a ];
then
echo "New version $b available"
echo "Enter y to download and anything else to quit..."
read choice;

if [$choice = y];
then
if [-e .git];
then
git pull
echo "Updated successfully"
git log --full-diff
else
filelist = cat filelists
for file in $filelist; do
rm file
wget https://github.com/MSF-Jarvis/AAC-Redux/archive/master.zip
unzip -r master.zip .
echo "Update completed successfully"
sleep (1.5)
./AAC-Redux.py
