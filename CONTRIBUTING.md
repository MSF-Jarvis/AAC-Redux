Adding new ROMs in AAC is really simple. All you want beforehand is the link to the ROMs manifest and branch names for different Android versions.


The procedure goes somewhat like this :

1. Fetch yourself the links mentioned above
2. Create copies of the classes popupCm_info and popupCm
3. Replace the image,info and links provided in the popupCm_info class as per the ROM you wanna add and rename the class itself to reflect the ROM, for e.g. DirtyUnicorns can be under popupDu_info
4. Then go over to the copy of the popupCm and replace the manifest link and button's text property in accordance with the ROM you are adding. Take care to rename the class in accordance with the above given nomenclature.
5. Navigate to the class popupRom and add an entry to your ROM, giving the command parameter as the name of the class you defined in step 4
6. Now find the class popupInfo(object) and add a similar entry for the ROM you are adding, with the command parameter pointing to the popupxx_info class you defined in step 3.
7. Bravo !!! You've successfully added a new ROM to AAC-Redux. Now you can go ahead and create a pull request to the original repository, which will be merged in two days at the latest and due credits will be added into the README
