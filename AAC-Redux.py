from tkinter import *
import os
import tkinter.messagebox
import webbrowser

def update():
    os.system('sh updater.sh')

def rom_init():
    input = popupRom(root)
    root.wait_window(input.top)

def cm_info():
    input = popupCm_info(root)
    root.wait_window(input.top)

def CM():
    webbrowser.open('http://www.cyanogenmod.org/')

def CM_git():
    webbrowser.open('https://github.com/CyanogenMod')

def CM_gerrit():
    webbrowser.open('https://jira.cyanogenmod.org/secure/Dashboard.jspa')

def cm_init():
    input = popupCm(root)
    root.wait_window(input.top)

def pa_info():
    input = popupPa_info(root)
    root.wait_window(input.top)

def AOSPA():
    webbrowser.open('http://paranoidandroid.co')

def AOSPA_git():
    webbrowser.open('https://GitHub.com/AOSPA')

def AOSPA_gerrit():
    webbrowser.open('http://gerrit.paranoidandroid.co')
def pa_init():
    input = popupPa(root)
    root.wait_window(input.top)

def aokp_info():
    input = popupAokp_info(root)
    root.wait_window(input.top)

def AOKP():
    webbrowser.open('http://aokp.co')

def AOKP_git():
    webbrowser.open('https://GitHub.com/AOKP')

def AOKP_gerrit():
    webbrowser.open('http://gerrit.aokp.co')

def aokp_init():
    input = popupAokp(root)
    root.wait_window(input.top)

def fix_dependencies():
    input = popupPack(root)
    root.wait_window(input.top)

def info():
    input = popupInfo(root)
    root.wait_window(input.top)

def get_repo():
    os.system("mkdir ~/bin")
    os.system("curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo && chmod a+x ~/bin/repo")

def get_java():
    os.system("sudo apt-add-repository ppa:webupd8team/java && sudo apt-get update && sudo apt-get install oracle-java7-installer")

def get_packs():
    os.system("sudo apt-get install git-core gnupg flex bison gperf build-essential \
    zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 \
    lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev ccache \
    libgl1-mesa-dev libxml2-utils xsltproc unzip lzop")

def get_all():
    os.system("curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo && chmod a+x ~/bin/repo")
    os.system("sudo apt-get install git-core gnupg flex bison gperf build-essential \
    zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 \
    lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev ccache \
    libgl1-mesa-dev libxml2-utils xsltproc unzip lzop")
    os.system("sudo apt-add-repository ppa:webupd8team/java && sudo apt-get update && sudo apt-get install oracle-java7-installer")
def sync():
    os.system("repo sync")

def build():
    prompt = tkinter.messagebox.askquestion("Prompt", "Please ensure that you have selected a ROM \n and synced sources before proceeding")
    if prompt == "yes":
        input = popupBuild(root)
        root.wait_window(input.top)
        build_actual()

def build_actual():
    os.system("source build/envsetup.sh && brunch "+device)

def contact():
    webbrowser.open('http://msfjarvis.tk/aac-redux')

def bugreport():
    webbrowser.open('https://github.com/MSF-Jarvis/AAC-Redux/issues')

def popupabout():
    input = popupAbout(root)
    root.wait_window(input.top)

class popupBuild(object):
    def __init__(self,master):
        top=self.top=Toplevel(master)
        self.l=Label(top,text="Enter codename of device to begin build")
        self.l.pack()
        device_name = ""
        self.b1=Entry(top,textvariable=device_name,width=30)
        self.b1.pack()
        self.b2=Button(top,text="OK",command=build_actual)
        self.b2.pack()
        global device
        device = device_name

class popupPack(object):
    def __init__(self,master):
        top=self.top=Toplevel(master)
        self.l=Label(top,text="Select what all do you need to Install... \n If in doubt, click All")
        self.l.pack()
        self.b1=Button(top,text="repo",command=get_repo)
        self.b1.pack()
        self.b2=Button(top,text="Java",command=get_java)
        self.b2.pack()
        self.b3=Button(top,text="Other dependencies...",command=get_packs)
        self.b3.pack()
        self.b4=Button(top,text="Get All",command=get_all)
        self.b4.pack()

class popupCm(object):
    def __init__(self,master):
        top=self.top=Toplevel(master)
        self.l=Label(top,text="Select the branch you want to sync...")
        self.l.pack()
        self.b1=Button(top,text="CM13.0 - MarshMallow",command=os.system("repo init -u git://github.com/CyanogenMod/android.git -b cm-13.0"))
        self.b1.pack()
        self.b2=Button(top,text="CM12.1 - Lollipop 5.1",command=os.system("repo init -u git://github.com/CyanogenMod/android.git -b cm-12.1"))
        self.b2.pack()
        self.b3=Button(top,text="CM12.0 - Lollipop 5.0",command=os.system("repo init -u git://github.com/CyanogenMod/android.git -b cm-12.0"))
        self.b3.pack()
        self.b4=Button(top,text="CM11 - KitKat",command=os.system("repo init -u git://github.com/CyanogenMod/android.git -b cm-11.0"))
        self.b4.pack()

class popupAokp(object):
    def __init__(self,master):
        top=self.top=Toplevel(master)
        self.l=Label(top,text="Select the branch you want to sync...")
        self.l.pack()
        self.b1=Button(top,text="AOKP Lollipop",command=os.system("repo init -u https://github.com/AOKP/platform_manifest.git -b lollipop"))
        self.b1.pack()
        self.b2=Button(top,text="AOKP KitKat",command=os.system("repo init -u https://github.com/AOKP/platform_manifest.git -b kitkat"))
        self.b2.pack()

class popupPa(object):
    def __init__(self,master):
        top=self.top=Toplevel(master)
        self.l=Label(top,text="Select the branch you want to sync...")
        self.l.pack()
        self.b1=Button(top,text="PA MarshMallow",command=os.system("repo init -u git://github.com/AOSPA/manifest.git -b marshmallow"))
        self.b1.pack()
        self.b2=Button(top,text="PA Lollipop 5.1",command=os.system("repo init -u git://github.com/AOSPA/manifest.git -b lollipop-mr1-rebase"))
        self.b2.pack()
        self.b3=Button(top,text="PA KitKat",command=os.system("repo init -u git://github.com/AOSPA/manifest.git -b kitkat"))
        self.b3.pack()

class popupPa_info(object):
    def __init__(self,master):
        top=self.top=Toplevel(master)
        pa_pic= PhotoImage(file="pa_pic.png")
        self.l=Label(top,image=pa_pic)
        self.l.image=pa_pic
        self.l.pack()
        self.l1=Label(top,text="Paranoid Android or AOSPA \n AOSPA offers you a clean UI with handy customisation features")
        self.l1.pack()
        self.b1=Button(top,text="AOSPA Website",command=AOSPA)
        self.b2=Button(top,text="AOSPA GitHub",command=AOSPA_git)
        self.b3=Button(top,text="AOSPA Gerrit",command=AOSPA_gerrit)
        self.b1.pack()
        self.b2.pack()
        self.b3.pack()

class popupCm_info(object):
    def __init__(self,master):
        top=self.top=Toplevel(master)
        cm_pic= PhotoImage(file="cm_pic.png")
        self.l=Label(top,image=cm_pic)
        self.l.image=cm_pic
        self.l.pack()
        self.l1=Label(top,text="CyanogenMod or CM \n CM is the king of all custom ROMs and base for most of them")
        self.l1.pack()
        self.b1=Button(top,text="CM Website",command=CM)
        self.b2=Button(top,text="CM GitHub",command=CM_git)
        self.b3=Button(top,text="CM Gerrit",command=CM_gerrit)
        self.b1.pack()
        self.b2.pack()
        self.b3.pack()

class popupAokp_info(object):
    def __init__(self,master):
        top=self.top=Toplevel(master)
        aokp_pic= PhotoImage(file="aokp_pic.png")
        self.l=Label(top,image=aokp_pic)
        self.l.image=aokp_pic
        self.l.pack()
        self.l1=Label(top,text="Android Open Kang Project or AOKP \n AOKP is insanely customisable and functional as well")
        self.l1.pack()
        self.b1=Button(top,text="AOKP Website",command=AOKP)
        self.b2=Button(top,text="AOKP GitHub",command=AOKP_git)
        self.b3=Button(top,text="AOKP Gerrit",command=AOKP_gerrit)
        self.b1.pack()
        self.b2.pack()
        self.b3.pack()
class popupRom(object):
    def __init__(self,master):
        top=self.top=Toplevel(master)
        self.l=Label(top,text="Select the ROM you want to sync...")
        self.l.pack()
        self.b1=Button(top,text="CyanogenMod",command=cm_init)
        self.b2=Button(top,text="Paranoid Android",command=pa_init)
        self.b3=Button(top,text="Android Open Kang Project",command=aokp_init)
        self.b1.pack()
        self.b2.pack()
        self.b3.pack()

class popupInfo(object):
    def __init__(self,master):
        top=self.top=Toplevel(master)
        self.l=Label(top,text="Select the ROM you want to know more about...")
        self.l.pack()
        self.b1=Button(top,text="CyanogenMod",command=cm_info)
        self.b2=Button(top,text="Paranoid Android",command=pa_info)
        self.b3=Button(top,text="Android Open Kang Project",command=aokp_info)
        self.b1.pack()
        self.b2.pack()
        self.b3.pack()

class popupAbout(object):
    def __init__(self,master):
        top=self.top=Toplevel(master)
        top.geometry("300x300")
        self.l=Label(top,text="Android Auto Compiler - Redux")
        self.l1=Label(top,text="v"+version)
        self.l2=Label(top,text=" \n \n \n \n")
        self.l3=Label(top,text="Credits")
        self.l4=Label(top,text="Original Idea : @AndroGeek974(Damien Boyer) \n Coded by : @MSF-Jarvis(Harsh Shandilya) \n External Contributions : @faddat(Jacob Gadikian)")
        self.l.pack()
        self.l1.pack()
        self.l2.pack()
        self.l3.pack()
        self.l4.pack()

class mainWindow(object):
    def __init__(self,master):
        self.master=master
        self.m=Menu(master)
        master.config(menu=self.m)
        self.sm=Menu(self.m)
        self.m.add_cascade(label="Menu", menu=self.sm)
        self.sm.add_command(label="Install packages", command=fix_dependencies)
        self.sm.add_command(label="Create ROM dir",command=rom_init)
        self.sm.add_command(label="Select ROM",command=rom_init)
        self.sm.add_separator()
        self.sm.add_command(label="Sync sources",command=sync)
        self.sm.add_command(label="Build ROM",command=build)
        self.im=Menu(self.m)
        self.m.add_cascade(label="Help", menu=self.im)
        self.im.add_command(label="Contact Devs",command=contact)
        self.im.add_command(label="Report a bug",command=bugreport)
        self.im.add_separator()
        self.im.add_command(label="About",command=popupabout)
        self.l=Label(master,text="Android Auto Compiler v"+version+"\n"+"By @MSF-Jarvis"+"\n"+"Original Concept by @AndroGeek974",bg="#10bf94")
        self.l.pack(fill="x")
        self.l1=Label(master,text=" \n \n \n \n \n")
        self.l1.pack()
        self.b=Button(master,text="Install required packages and applications",command=fix_dependencies,bg="#e0c11c")
        self.b.pack()
        self.b1=Button(master,text="Know more about ROMs on AAC",command=info,bg="#e0c11c")
        self.b1.pack()
        self.b2=Button(master,text="Select ROM to sync sources",command=rom_init,bg="#e0c11c")
        self.b2.pack()
        self.b3=Button(master,text="Sync the sources",command=sync,bg="#e0c11c")
        self.b3.pack()
        self.b4=Button(master,text="Build ROM",command=build,bg="#e0c11c")
        self.b4.pack()
        self.l2=Label(master,text="\n \n \n \n \n \n")
        self.l2.pack()
        self.b5=Button(master,text="Update AAC-Redux",command=update,bg='#e0c11c')
        self.b5.pack()


if __name__ == "__main__":
    version="1.0_25112015"
    root=Tk()
    root.geometry("400x440")
    root.title("AAC-Redux v"+version)
    root.configure(background="#a4073f")
    m=mainWindow(root)
    root.mainloop()
